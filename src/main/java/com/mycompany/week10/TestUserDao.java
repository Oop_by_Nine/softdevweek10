/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.week10;

import com.mycompany.week10.dao.UserDao;
import com.mycompany.week10.helper.DatabaseHelper;
import com.mycompany.week10.model.User;


/**
 *
 * @author Admin
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        User user1 = userDao.get(1);
//       System.out.println(user1);
//
//        User newUser = new User("extra", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        
//        user1.setName("namotussa");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser+"xcx");
        userDao.delete(user1);
        for (User u : userDao.getAll("user_name like 'm%' ","user_name asc , user_gender desc ")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }
}
