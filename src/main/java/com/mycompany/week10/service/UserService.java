/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.week10.service;

import com.mycompany.week10.dao.UserDao;
import com.mycompany.week10.model.User;

/**
 *
 * @author Admin
 */
public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user=  userDao.getByName(name);
        try{
        if(user!=null&&user.getPassword().equals(password)){
            return user;
        }
        }catch(Exception ex){
            System.out.println("error");
        }
        return null;
    }
}
