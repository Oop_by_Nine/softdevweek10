/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.week10;

import com.mycompany.week10.model.User;
import com.mycompany.week10.service.UserService;

/**
 *
 * @author Admin
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("muangsiri", "password");
        if(user!=null){
            System.out.println("Welcome : "+user.getName());
        }
    }
}
